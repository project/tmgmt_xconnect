<?php

/**
 * @file
 * Contains \Drupal\tmgmt_xconnect\Exception\ImportException.
 */

namespace Drupal\tmgmt_xconnect\Exception;

/**
 * A base exception thrown by failing import operations.
 */
class ImportException extends \RuntimeException {}
